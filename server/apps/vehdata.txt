--@name VehicleDataApp (SERVER)
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

local app = {
	Update = function(ply)
		if (ply:inVehicle() and not(globaltables.global.IntelliHUD == nil)) then
			local veh = ply:vehicle()
			if (IsValid(veh) and not(globaltables.global.IntelliHUD[veh:index()] == nil)) then
				local data = globaltables.global.IntelliHUD[veh:index()]
				if (IsValid(data.ZPM) and data.ZPM:class() == "starfall_processor") then
					CheckVar("Name", data.Name)
					CheckVar("Energy", data.ZPM:getWirelink()["P"])
					return
				end
			end
		end
		CheckVar("Name", nil)
	end
}

HUD.DefineApp("VehicleData", app, true)