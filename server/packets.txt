--@name Lanteans HUD ServerPackets
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

net.RegPacket(0, "LINK", function()
	local p = net.readEntity()
	chat.listen(PlayerChatFunc, p)
	PlysData[p] = {AppsData = {}, Apps = CurApps}
	for k, v in pairs(PlysData[p].Apps) do
		if (not(v.OnActivated == nil)) then
			v.OnActivated(p)
		end
	end
end, nil, true)

net.RegPacket(1, "UNLINK", function()
	local p = net.readEntity()
	chat.stop(PlayerChatFunc, p)
	for k, v in pairs(PlysData[p].Apps) do
		if (not(v.OnDeactivated == nil)) then
			v.OnDeactivated(p)
		end
	end
	PlysData[p] = nil
end, nil, true)

net.RegPacket(2, "OPENCONFIG", nil, function()
end, true)

net.RegPacket(3, "APP_ON", function(ply)
	if (PlysData[ply] == nil) then
		print("Starfall bug detected : Starfall hud initializes client side without anyone linked !")
		return
	end
	local name = net.readString()
	HUD.ActivateApp(name, true, ply)
end, nil, true)

net.RegPacket(4, "APP_OFF", function(ply)
	if (PlysData[ply] == nil) then
		print("Starfall bug detected : Starfall hud initializes client side without anyone linked !")
		return
	end
	local name = net.readString()
	HUD.ActivateApp(name, false, ply)
end, nil, true)

net.RegPacket(5, "APP_UPDATE", nil, function(args)
	net.writeString(args[1])
	net.writeTable(args[2])
end, false)