--@name StargateInfoApp (CLIENT)
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

local SGType = nil
local SGName = nil
local SGIris = false
local SGOwner = nil
local SGAddress = nil
local SGTargetA = nil
local SGTargetN = nil
local SGIn = false
local SGModel = nil

local mat = Material("color")

local WhiteList = {
	["stargate_atlantis"] = render.createClientModel("models/madman07/stargate/base_atlantis.mdl", render.RENDERGROUP_OPAQUE),
	["stargate_universe"] = render.createClientModel("models/the_sniper_9/universe/stargate/universegate.mdl", render.RENDERGROUP_OPAQUE),
	["stargate_tollan"] = render.createClientModel("models/alexalx/stargate_cebt/sgtbase.mdl", render.RENDERGROUP_OPAQUE),
	["stargate_sg1"] = render.createClientModel("models/madman07/stargate/base.mdl", render.RENDERGROUP_OPAQUE)
}

for k, v in pairs(WhiteList) do
	v:setMaterial("models/wireframe")
end

local function HandleKeyPress(ply, key)
	if (key == input.IN_WALK and IsValid(ply:activeWeapon()) and ply:activeWeapon():class() == "kinoremote") then
		local ent = ply:eyeTrace().Entity
		if (IsValid(ent) and WhiteList[ent:class()]) then
			if (not(SGModel)) then
				SGModel = WhiteList[ent:class()]
			end
		end
	end
end

local function HandleKeyRelease(ply, key)
	if (key == input.IN_WALK and IsValid(ply:activeWeapon()) and ply:activeWeapon():class() == "kinoremote") then
		if (SGModel) then
			SGModel = nil
		end
	end
end

local app = {
	OnActivated = function()
		hook("keypress", "LanteansHUD_SGInfo", HandleKeyPress)
		hook("keyrelease", "LanteansHUD_SGInfo", HandleKeyRelease)
	end,
	OnDeactivated = function()
		hook.remove("keypress", "LanteansHUD_SGInfo")
		hook.remove("keyrelease", "LanteansHUD_SGInfo")
	end,
	HasNetwork = true,
	OverrideList = {},
	Render = function()
		if (not(SGType == nil)) then
			local y = ScrH / 2 - 128
			GUI.InvGradiantRect(ScrW / 2 - 150, ScrH / 2 - 128, 300, 256, COLOR(0, 0, 0))
			GUI.NormGradiantRect(ScrW / 2 - 150, ScrH / 2 - 128, 300, 256, COLOR(0, 0, 0))
			--Type
			local w = GUI.TextSize(SGType, SmallFont)
			GUI.Text(SGType, ScrW / 2 - w / 2, y + 10, SmallFont, COLOR(255, 255, 255))
			--Owner
			local txt = "Owner : " .. SGOwner
			w = GUI.TextSize(txt, SmallFont)
			GUI.Text(txt, ScrW / 2 - w / 2, y + 40, SmallFont, COLOR(255, 255, 255))
			--Name
			txt = SGName
			w = GUI.TextSize(txt, SmallFont)
			GUI.Text(txt, ScrW / 2 - w / 2, y + 60, SmallFont, COLOR(255, 255, 255))
			--Address
			txt = "(" .. SGAddress .. ")"
			w = GUI.TextSize(txt, SmallFont)
			GUI.Text(txt, ScrW / 2 - w / 2, y + 80, SmallFont, COLOR(255, 255, 255))

			--Target--
			txt = "Target"
			w = GUI.TextSize(txt, SmallFont)
			GUI.Text(txt, ScrW / 2 - w / 2, y + 110, SmallFont, COLOR(255, 255, 255))
			--Name
			txt = SGTargetN
			w = GUI.TextSize(txt, SmallFont)
			GUI.Text(txt, ScrW / 2 - w / 2, y + 130, SmallFont, COLOR(255, 255, 255))
			--Address
			txt = "(" .. SGTargetA .. ")"
			w = GUI.TextSize(txt, SmallFont)
			GUI.Text(txt, ScrW / 2 - w / 2, y + 150, SmallFont, COLOR(255, 255, 255))

			if (SGIn or SGIris) then
				txt = "! UNSAFE GATE !"
				w = GUI.TextSize(txt, SmallFont)
				GUI.Text(txt, ScrW / 2 - w / 2, y + 180, SmallFont, COLOR(255, 0, 0))
			end
			if (SGModel) then
				render.setMaterial(mat)
				render.start3D()
					render.suppressEngineLighting(true)
					SGModel:setPos(LocalPlayer:pos() + LocalPlayer:forward() * 2000 + LocalPlayer:up() * 700)
					SGModel:setAng(LocalPlayer:getAngles())
					SGModel:setScaleVector(Vector(2, 2, 2))
					SGModel:drawModel()
					render.suppressEngineLighting(false)
				render.end3D()
			end
		end
	end,
	OnDataReceived = function(data)
		SGType = data.SGType
		SGName = data.SGName
		SGIris = data.SGIris
		SGIn = data.SGIn
		SGOwner = data.SGOwner
		SGAddress = data.SGAddr
		SGTargetA = data.SGTargetA
		SGTargetN = data.SGTargetN
	end
}

HUD.DefineApp("StargateInfo", app, true)
