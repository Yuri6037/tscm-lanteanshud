--@name VehicleHorizonApp (CLIENT)
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

local app = {
	HasNetwork = false,
	OverrideList = {},
	Render = function()
		if (LocalPlayer:inVehicle()) then
			local veh = LocalPlayer:vehicle()
			local ang = LocalPlayer:ang()
			local pitch, yaw, roll = ang:RotateAroundAxis(ang:Right(), 90):Unpack()

			local mat = Matrix()
			local pos = Vector(0.5, 0.5)
			local real = Vector(ScrW / 2, 100)
			mat:translate(pos + real)
			mat:rotate(Angle(0, roll, 0))
			mat:scale(Vector(128, 2))
			mat:translate(-(pos + real))
			render.pushMatrix(mat)
			GUI.ColoredRect(pos.X + real.X, pos.Y + real.Y, 1, 1, COLOR(255, 255, 255))
			render.popMatrix()
			GUI.ColoredRect(ScrW / 2 - 32, ScrH / 2 + pitch, 64, 2, COLOR(255, 255, 255))
			GUI.ColoredRect(ScrW / 2 + yaw, ScrH / 2 - 32, 2, 64, COLOR(255, 255, 255))
		end
	end
}

HUD.DefineApp("VehicleHorizon", app, true)
