--@name Lanteans HUD ClientMain
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

Self = ents.self()
LocalPlayer = ents.player()
ScrW, ScrH = render.getScreenRes()

SmallFont = GUI.Font("federation", 18, true)
MediumFont = GUI.Font("federation", 24, true)
LargeFont = GUI.Font("federation", 32, true)

--App Specification
--[[
OverrideList = {
	"defaulthudtoremove"
}
function Render()
OPTIONAL function OnActivated()
OPTIONAL function OnDeactivated()
]]--
--End

--Register the permission to create and play sounds
permissions.register( { permissions.PLAY_CLIENT_SOUNDS } )

local AppCnt = 0
local Apps = {}
local CurApps = {}
local RMHuds = {}

HUD = {}
function HUD.DefineApp(name, app, active)
	Apps[name] = app
	AppCnt = AppCnt + 1
	if (active) then HUD.ActivateApp(name, true) end
end
function HUD.ActivateApp(name, b)
	if (Apps[name] == nil) then error("No such application found !") end
	if (b) then
		CurApps[name] = Apps[name]
		if (Apps[name].OnActivated) then
			Apps[name].OnActivated()
		end
		for k, v in pairs(Apps[name].OverrideList) do
			RMHuds[v] = true
		end
		if (Apps[name].HasNetwork) then
			net.SendPacket("APP_ON", name)
		end
	else
		CurApps[name] = nil
		if (Apps[name].OnDeactivated) then
			Apps[name].OnDeactivated()
		end
		for k, v in pairs(Apps[name].OverrideList) do
			RMHuds[v] = nil
		end
		if (Apps[name].HasNetwork) then
			net.SendPacket("APP_OFF", name)
		end
	end
end
function HUD.NetworkEvent(name, tbl)
	if (not(CurApps[name] == nil)) then
		CurApps[name].OnDataReceived(tbl)
	end
end
function HUD.AppActive(name)
	return not(CurApps[name] == nil)
end
function HUD.OpenConfigPanel()
	local f = vgui.create("DFrame")
	f.OnKeyCodeReleased = function(fuck, code)
		if (code == input.KEY_F3) then f:Close() return end
	end
	f:SetTitle("Lanteans HUD - Config (V." .. VERSION .. ")")
	f:SetSize(450, AppCnt * 50)
	f.Paint = function(self, w, h)
		render.setColor(0, 0, 0, 128)
		render.drawRect(0, 0, w, h)
		render.setColor(0, 128, 255)
		render.drawLine(0, 0, w, 0)
		render.drawLine(w - 1, 0, w - 1, h)
		render.drawLine(0, h - 1, w - 1, h - 1)
		render.drawLine(0, 0, 0, h)
	end
	f:Center()
	f:MakePopup()
	local y = 32
	for k, v in pairs(Apps) do
		local lbl = vgui.create("DLabel", f)
		lbl:SetPos(20, y)
		lbl:SetSize(492, 32)
		lbl:SetText(k)
		lbl:SetFont(SmallFont)
		if (HUD.AppActive(k)) then
			lbl:SetTextColor(Color(0, 255, 255))
		else
			lbl:SetTextColor(Color(255, 255, 255))
		end
		lbl:SetMouseInputEnabled(true)
		lbl.DoClick = function()
			HUD.ActivateApp(k, not(HUD.AppActive(k)))
			if (HUD.AppActive(k)) then
				lbl:SetTextColor(Color(0, 255, 255))
			else
				lbl:SetTextColor(Color(255, 255, 255))
			end
		end
		y = y + 32
	end
end

hook("hudshoulddraw", "LanteansHUD_DisableHUD", function(elem)
	if (RMHuds[elem]) then return true end
end)

hook("render", "LanteansHUD_Render", function()
	for k, v in pairs(CurApps) do
		v.Render()
	end
end)

hook("hudlink", "LanteansHUD_HudLink", function(ply)
	net.SendPacket("LINK", ply)
end)

hook("hudunlink", "LanteansHUD_HudUnlink", function(ply)
	net.SendPacket("UNLINK", ply)
end)

hook("playerbindpress", "LanteansHUD_BindPress", function(ply, bind, pressed)
	if (not(hud.isLinked())) then return end
	if (ply == LocalPlayer and bind == "gm_showspare1") then
		HUD.OpenConfigPanel()
	end
end)